package com.maker.geodbusers.view

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.TextUtils
import com.maker.geodbusers.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val prefs = PreferenceManager.getDefaultSharedPreferences(this);
        val token = prefs.getString(UserListFragment.TOKEN, "")
        val intent: Intent
        if (TextUtils.isEmpty(token)){
            intent = Intent(this, LoginActivity::class.java)
        }else{
            intent = Intent (this, UserListActivity::class.java)
            intent.putExtra(UserListFragment.TOKEN, token);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }
}
