package com.maker.geodbusers.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.maker.geodbusers.R
import com.maker.geodbusers.viewmodels.UsersViewModel

class UserListFragment : Fragment(){
    companion object {

        var TAG = "UserListFragment"
        var TOKEN = "token"

        fun newInstance(token: String?): UserListFragment {
            val args: Bundle = Bundle()
            args.putString(TOKEN, token)
            val fragment = UserListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var refreshLayout: SwipeRefreshLayout
    lateinit var userRecycler: RecyclerView
    lateinit var viewModel: UsersViewModel
    lateinit var adapter: UserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(UsersViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val v = inflater.inflate(R.layout.fragment_users, container, false)
        refreshLayout = v.findViewById(R.id.refresh_srl)
        userRecycler = v.findViewById(R.id.users_rv)
        userRecycler.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        adapter = UserAdapter(activity!!)
        userRecycler.adapter = adapter
        var token = arguments?.getString(TOKEN)
        token = if (token == null) "" else token
        viewModel.init(token)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getNewsRepository()?.observe(this, Observer {
            if (it == null) {
                //TODO this is not correct to be handled this way, need proper error mangement
                onError()
            }else {
                adapter.set(it)
                adapter.notifyDataSetChanged()
            }
        })
        refreshLayout.setOnRefreshListener {
            refresh()
        }
    }

    fun onError(){
        if (isAdded)
            Snackbar.make(view!!, getString(R.string.get_users_error), Snackbar.LENGTH_SHORT).show()
    }

    fun refresh(){

    }

}

