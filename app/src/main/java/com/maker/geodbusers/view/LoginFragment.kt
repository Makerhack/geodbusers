package com.maker.geodbusers.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.maker.geodbusers.R
import com.maker.geodbusers.viewmodels.TokenViewModel

class LoginFragment : Fragment() {

    companion object {

        var TAG = "LoginFragment"

        fun newInstance(): LoginFragment {
            val args: Bundle = Bundle()
            val fragment = LoginFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var model: TokenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this).get(TokenViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val v = inflater.inflate(R.layout.fragment_login, container, false)
        val loginBtn = v.findViewById<Button>(R.id.btn_login)
        val emailEt = v.findViewById<EditText>(R.id.input_email)
        val passwordEt = v.findViewById<EditText>(R.id.input_password)
        loginBtn.setOnClickListener { model.login(emailEt.text.toString(),
            passwordEt.text.toString())?.observe(this, Observer {
            if(it.token == null){
                onError();
            }else{
                onSuccess(it.token)
            }
        }) }
        return v
    }

    fun onSuccess(token: String){
        val intent = Intent(activity, UserListActivity::class.java)
        intent.putExtra(UserListFragment.TOKEN, token)
        startActivity(intent)
    }

    fun onError(){
        if (isAdded)
        Snackbar.make(view!!, getString(R.string.login_error), Snackbar.LENGTH_SHORT).show()
    }
}