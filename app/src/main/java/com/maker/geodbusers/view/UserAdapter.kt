package com.maker.geodbusers.view

import android.content.Context
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.maker.geodbusers.R
import com.maker.geodbusers.adapter.ItemViewHolder
import com.maker.geodbusers.base.ItemRecyclerAdapter
import com.maker.geodbusers.model.User
import com.squareup.picasso.Picasso

class UserAdapter(context: Context) : ItemRecyclerAdapter<User, UserAdapter.UserHolder>(context) {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): UserHolder {
        val v: View = layoutInflater.inflate(R.layout.row_user, null, false)
        return UserHolder(v)
    }

    override fun onBindViewHolder(holder: UserHolder, position: Int, item: User) {
        holder.nameTv.setText(item.firstName)
        holder.lastNameTv.setText(item.lastName)
        Picasso.get().load(item.avatarSmall).into(holder.avatarIv)
        holder.itemView.setOnClickListener {
            val intent = Intent(context, UserDetailActivity::class.java)
            intent.putExtra(UserDetailActivity.USER, item);
            context.startActivity(intent)
        }
    }


    class UserHolder(itemView: View) : ItemViewHolder<User>(itemView) {
        var avatarIv: ImageView = itemView.findViewById(R.id.avatar_iv)
        var nameTv: TextView = itemView.findViewById(R.id.name_tv)
        var lastNameTv: TextView = itemView.findViewById(R.id.lastname_tv)

    }
}