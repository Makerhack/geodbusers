package com.maker.geodbusers.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class UserListActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            var token = intent?.extras?.getString(UserListFragment.TOKEN, "")
            val ft = supportFragmentManager.beginTransaction();
            ft.replace(android.R.id.content,
                UserListFragment.newInstance(token),
                UserListFragment.TAG
            )
        }
    }
}