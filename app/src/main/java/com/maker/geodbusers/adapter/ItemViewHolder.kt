package com.maker.geodbusers.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.maker.geodbusers.base.ItemRecyclerAdapter

/**
 * Viewholder class that has a reference to the cell data element
 * Created by Santi on 10/04/2015.
 */
abstract class ItemViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    protected var item: T? = null
    protected var mPosition :Int= 0

    fun bindItem(item: T, position: Int, listener: ItemRecyclerAdapter.OnItemClickListener<T>?) {
        this.item = item
        this.mPosition = position
        itemView.setOnClickListener {
            listener?.onItemClick(item, itemView, position)
        }
    }
}
