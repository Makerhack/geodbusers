package com.maker.geodbusers.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("FirstName") val firstName: String,
    @SerializedName("LastName") val lastName: String,
    @SerializedName("Email") val email: String,
    @SerializedName("Gender") val gender: String,
    @SerializedName("AvatarSmall") val avatarSmall: String,
    @SerializedName("AvatarBig") val avatarBig: String
) : Parcelable {

    constructor(parcelable: Parcel) : this (
        parcelable.readString(),
        parcelable.readString(),
        parcelable.readString(),
        parcelable.readString(),
        parcelable.readString(),
        parcelable.readString())

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(firstName)
        dest?.writeString(lastName)
        dest?.writeString(email)
        dest?.writeString(gender)
        dest?.writeString(avatarSmall)
        dest?.writeString(avatarBig)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}