package com.maker.geodbusers.viewmodels

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.maker.geodbusers.model.TokenResponse
import com.maker.geodbusers.model.User
import com.maker.geodbusers.repository.UserRepository


class TokenViewModel : ViewModel() {

    private var mutableLiveData: MutableLiveData<TokenResponse>? = null
    private val userRepository: UserRepository = UserRepository.instance


    fun login(user: String, pass: String): LiveData<TokenResponse>? {
        if (mutableLiveData == null)
            mutableLiveData = userRepository.login(user, pass)
        return mutableLiveData
    }

}