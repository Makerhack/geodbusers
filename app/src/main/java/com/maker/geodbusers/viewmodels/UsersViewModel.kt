package com.maker.geodbusers.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.maker.geodbusers.model.User
import com.maker.geodbusers.repository.UserRepository


class UsersViewModel : ViewModel() {

    private var mutableLiveData: MutableLiveData<List<User>>? = null
    private val userRepository: UserRepository = UserRepository.instance

    fun init(token: String) {
        if (mutableLiveData != null) {
            return
        }
        mutableLiveData = userRepository.getUsers(token)

    }

    fun getNewsRepository(): LiveData<List<User>>? {
        return mutableLiveData
    }

}