package com.maker.geodbusers.repository.network

import com.maker.geodbusers.model.TokenResponse
import com.maker.geodbusers.model.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface UsersApi {

    @POST("login")
    fun login(@Query("user") user:String, @Query("pass") pass:String): Call<TokenResponse>

    @GET("users/")
    fun getUsers(@Query("token") token:String): Call<List<User>>

}