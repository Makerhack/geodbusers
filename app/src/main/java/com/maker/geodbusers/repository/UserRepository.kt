package com.maker.geodbusers.repository

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.lifecycle.MutableLiveData
import com.maker.geodbusers.model.TokenResponse
import com.maker.geodbusers.model.User


import com.maker.geodbusers.repository.network.RetrofitService
import com.maker.geodbusers.repository.network.UsersApi
import com.maker.geodbusers.view.UserListFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepository() {

    private val usersApi: UsersApi = RetrofitService.createService(UsersApi::class.java)

    fun getUsers(token: String): MutableLiveData<List<User>> {
        val usersData = MutableLiveData<List<User>>()
        usersApi.getUsers(token).enqueue(object : Callback<List<User>> {
            override fun onResponse(
                call: Call<List<User>>,
                response: Response<List<User>>
            ) {
                if (response.isSuccessful) {
                    usersData.value = response.body()
                }
            }

            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                usersData.value = null
            }
        })
        return usersData
    }


    fun login(user: String, pass: String): MutableLiveData<TokenResponse> {
        val tokenData = MutableLiveData<TokenResponse>()
        usersApi.login(user, pass).enqueue(object : Callback<TokenResponse> {
            override fun onResponse(
                call: Call<TokenResponse>,
                response: Response<TokenResponse>
            ) {
                if (response.isSuccessful) {
                    tokenData.value = response.body()
                    //TODO store the token in shareprefs
                }
            }

            override fun onFailure(call: Call<TokenResponse>, t: Throwable) {
                tokenData.value = null
            }
        })
        return tokenData
    }

    companion object {

        private var userRepository: UserRepository? = null

        val instance: UserRepository
            get() {
                if (userRepository == null) {
                    userRepository = UserRepository()
                }
                return userRepository!!
            }
    }
}