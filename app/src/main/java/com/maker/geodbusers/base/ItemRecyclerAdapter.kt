package com.maker.geodbusers.base


import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maker.geodbusers.adapter.ItemViewHolder

import java.util.LinkedList

abstract class ItemRecyclerAdapter<T, VH : ItemViewHolder<T>>(protected val context: Context) :
    RecyclerView.Adapter<VH>() {
    protected val layoutInflater: LayoutInflater
    private var listener: OnItemClickListener<T>? = null
    private val dataList = LinkedList<T>()

    val items: List<T>
        get() = dataList

    init {
        layoutInflater = LayoutInflater.from(context)
    }

    fun clear() {
        dataList?.clear()
    }

    abstract override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): VH

    override fun onBindViewHolder(vh: VH, position: Int) {
        val item = dataList[position]
        vh.bindItem(item, position, listener)
        onBindViewHolder(vh, position, item)
    }

    abstract fun onBindViewHolder(holder: VH, position: Int, item: T)

    fun getItem(position: Int): T {
        return dataList[position]
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun set(objectList: List<T>?) {
        if (objectList != null) {
            this.dataList.clear()
            this.dataList.addAll(objectList)
            notifyDataSetChanged()
        }
    }

    @JvmOverloads
    fun add(`object`: T, position: Int = dataList.size) {
        if (dataList != null) {
            this.dataList.add(position, `object`)
            notifyDataSetChanged()
        }
    }

    @JvmOverloads
    fun addAll(objectList: List<T>?, position: Int = dataList.size) {
        if (objectList != null) {
            this.dataList.addAll(position, objectList)
            notifyDataSetChanged()
        }
    }

    fun remove(`object`: T?) {
        if (`object` != null) {
            val removed = this.dataList.remove(`object`)
            Log.d(javaClass.simpleName, "removed: $removed")
            notifyDataSetChanged()
        }
    }

    fun removeAll(objectList: List<T>?) {
        if (objectList != null) {
            this.dataList.removeAll(objectList)
            notifyDataSetChanged()
        }
    }

    fun setOnItemClickListener(listener: OnItemClickListener<T>) {
        this.listener = listener
    }

    interface OnItemClickListener<T> {
        fun onItemClick(item: T, rowView: View, position: Int)
    }
}